﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LittlePersonInfoPanel : MonoBehaviour 
{

	string currentStatus;

	void Start()
	{

	}
	void Update()
	{
		if( LittlePeopleClickHandler.selectedPerson != null ) {

			LittlePeople littlePerson = LittlePeopleClickHandler.selectedPerson.GetComponent<LittlePeople>();
			transform.FindChild("PlayerName").GetComponent<Text>().text = littlePerson.realName;

			transform.FindChild("HungerLabel").FindChild("HungerSlider").GetComponent<Slider>().value = littlePerson.hunger;
			if(littlePerson.hunger >= littlePerson.hungerSearchForFoodLimit)
			{
				transform.FindChild("HungerLabel").FindChild("HungerSlider").GetComponent<Image>().color = Color.red;
			} else { 
				transform.FindChild("HungerLabel").FindChild("HungerSlider").GetComponent<Image>().color = Color.white;
			}

			if(littlePerson.currentStatus == LittlePeopleStatus.WALKING)
			{
				if(littlePerson.transform.parent.GetComponent<Movement>().targetBuilding == null)
				{
					currentStatus = littlePerson.currentStatus.ToString() + " TO SOMEWHERE";
				}else{
					currentStatus = littlePerson.currentStatus.ToString() + " TO " + littlePerson.transform.parent.GetComponent<Movement>().targetBuilding.name;
				}
			} else 
			{
				currentStatus = littlePerson.currentStatus.ToString();
			}

			transform.FindChild("StatusPanel").FindChild("Status").GetComponent<TextSlider>().setText(currentStatus);

			transform.FindChild("EnergyLabel").FindChild("EnergySlider").GetComponent<Slider>().value = littlePerson.energy;
		}
		else {
			gameObject.SetActive(false);
		}
	}


}
