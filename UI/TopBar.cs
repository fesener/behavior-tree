﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TopBar : MonoBehaviour {

	Text populationText;
	GameManager manager;

	void Start () {
		manager = (GameManager)GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
		populationText = transform.FindChild("PopulationLabel").FindChild("Population").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		populationText.text = manager.littlePeople.Count.ToString();
	}
}
