﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour 
{

	public List<Transform> placesToLive {get; set;}
	public List<Transform> placesToEat {get; set;}
	public List<Transform> placesToWork {get; set;}
	public List<Transform> forests {get;set;}

	public List<GameObject> littlePeople {get; set;}

	public GameObject createNewPersonButton;
	public GameObject[] littlePersonPrefabs;

	public static GameObject littlePersonInfoPanel;

	void Awake()
	{
		littlePersonInfoPanel = GameObject.FindGameObjectWithTag("LittlePersonInfoPanel").gameObject;
		littlePersonInfoPanel.SetActive(false);

		littlePeople = new List<GameObject>();
		placesToEat = new List<Transform>();
		placesToLive = new List<Transform>();
		placesToWork = new List<Transform>();
		forests = new List<Transform>();



	}

	void Start()
	{
		createNewPersonButton.GetComponent<Button>().onClick.AddListener(() => { createNewPerson(); }); 
	}

	public void createNewPerson()
	{
		for(int i=0; i<100; i++)
		{
			GameObject go = (GameObject) Instantiate(littlePersonPrefabs[Random.Range(0,littlePersonPrefabs.Length)], new Vector3(Random.Range(-40,40),0,Random.Range(-40,40)), Quaternion.identity);
			littlePeople.Add(go);
		}
	}

	public Transform findClosestPlaceToEat(Vector3 position)
	{
		if(placesToEat.Count == 0) return null;

		Transform result = null;
		float minDistance = float.MaxValue;

		foreach(Transform t in placesToEat)
		{
			float dist = (t.gameObject.transform.position - position).magnitude;
			if(dist < minDistance)
			{
				minDistance = dist;
				result = t;
			}
		}
		return result;
	}
	public Transform findClosestForest(Vector3 position)
	{
		if(forests.Count == 0) return null;
		
		Transform result = null;
		float minDistance = float.MaxValue;
		
		foreach(Transform t in forests)
		{
			float dist = (t.gameObject.transform.position - position).magnitude;
			if(dist < minDistance)
			{
				minDistance = dist;
				result = t;
			}
		}
		return result;
	}

}
