﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class LittleHouse : MonoBehaviour {

	GameManager manager;
	public int capacity = 2;
	List<Transform> people = new List<Transform>();
	Text textboardText;

	// Use this for initialization
	void Start () {
		manager = (GameManager)GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
		manager.placesToLive.Add(this.transform);
		textboardText = transform.FindChild("Textboard").FindChild("Text").GetComponent<Text>();
	}
	
	public bool moveToHouse(Transform person)
	{
		if(people.Count < capacity)
		{
			people.Add(person);
			if(people.Count == capacity)
			{
				textboardText.text = textboardText.text + "(FULL)";
			}
			return true;
		}
		return false;
	}

	// Update is called once per frame
	void Update () {
	
	}
}
