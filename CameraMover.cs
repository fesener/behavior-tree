﻿using UnityEngine;
using System.Collections;

public class CameraMover : MonoBehaviour {
	
	Vector3 lastMousePos;
	float cursorSpeed = 150f;
	
	float zoomSpeed = 0.50f;
	float maxZoom = 150f;
	float minZoom = 15f;
	
	// Update is called once per frame
	void Update () {
		Vector3 dir = new Vector3( Input.GetAxis("Horizontal")*cursorSpeed, 0, Input.GetAxis("Vertical")*cursorSpeed);
		
		dir = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0) * dir;
		
		dir *= Time.deltaTime;
		
		Vector3 mousePos = Input.mousePosition;
		
		if(Camera.main.orthographic == false)
			mousePos.z = Camera.main.transform.position.y+10f;
		
		// Start Drag
		if( Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2) ) {
			lastMousePos = Camera.main.ScreenToWorldPoint( mousePos );
		}
		
		// Do Drag
		if( Input.GetMouseButton(1) || Input.GetMouseButton(2) ) {
			Vector3 diff = Camera.main.ScreenToWorldPoint(mousePos) - lastMousePos;
			transform.position += new Vector3(-diff.x, 0, -diff.z);
			lastMousePos = Camera.main.ScreenToWorldPoint( mousePos );
			//			Debug.Log(lastMousePos);
		}
		
		transform.position += dir;
		
		/*transform.position = new Vector3(
			Mathf.Clamp(transform.position.x, minX, maxX),
			transform.position.y,
			Mathf.Clamp(transform.position.z, minZ, maxZ)
			);*/
		
		// Do Zoom
		if(Camera.main.orthographic == true) {
			Camera.main.orthographicSize *= 1f + (Input.GetAxis("Mouse ScrollWheel") * -zoomSpeed);
			Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, minZoom, maxZoom);
		}
		else {
			Vector3 pos = Camera.main.transform.position;
			if( (Input.GetAxis("Mouse ScrollWheel") < 0 && pos.y < 1000) || (Input.GetAxis("Mouse ScrollWheel") > 0 && pos.y > 60)) {
				pos += Camera.main.transform.forward * (Input.GetAxis("Mouse ScrollWheel") * 400f);
				Camera.main.transform.position = pos;
			}
		}
	}
}
