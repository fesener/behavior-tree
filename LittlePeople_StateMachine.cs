﻿using UnityEngine;
using System.Collections;
using System;


public class LittlePeople_StateMachine : MonoBehaviour {

	public enum LittlePeopleStates
	{
		idle,
		walking,
		sleeping,
		eating,
		working
	}

	LittlePeopleStates currentState = LittlePeopleStates.idle;

	const float COOLDOWN = .5f;

	string alias;
	int age;
	DateTime dob;
	
	
	float religion;
	float fun;

	bool isSick { 
		set {
			if(value)
			{
				hungerRate = 0.5f;
				energyRate = 2f;
			}
		}
	}

	public float intervalCooldown;

	public float initialHunger = 100f;
	float hunger;
	float maxHunger = 100f;
	float hungerRate = 1f;

	public float initialEnergy = 100f;
	float energy;
	float maxEnergy = 100f;
	float energyRate = 1f;

	public float initialHealth = 100f;
	float health;
	float maxHealth = 100f;
	float healthRate = 1f;


	void Start () {
		intervalCooldown = COOLDOWN;

		energy = initialEnergy;
		hunger = initialHunger;
		health = initialHealth;
	}

	public void clickAction()
	{
		Debug.Log ("Hello world!");
	}
	

	void Update () 
	{
		intervalCooldown -= Time.deltaTime;
		if(intervalCooldown <= 0)
		{
			switch (currentState) 
			{
			case LittlePeopleStates.idle:
				adjustHunger(-1f);
				adjustEnergy(-1f);
				break;
			case LittlePeopleStates.eating:
				adjustEnergy(-0.5f);
				adjustHunger(5f);
				break;
			case LittlePeopleStates.sleeping:
				adjustEnergy(1f);
				adjustHunger(-0.5f);
				break;
			}

			intervalCooldown = COOLDOWN;
			printStats ();
		}

	}
	

	void adjustHunger(float rateCoefficient = 1f)
	{
		hunger += hungerRate * rateCoefficient;
		if(hunger <= 90){
			changeState(LittlePeopleStates.eating);
		} else if(hunger >= maxHunger) {
			hunger = maxHunger;
			changeState(LittlePeopleStates.idle);
		}
	}
	
	void adjustEnergy(float rateCoefficient = 1f)
	{
		energy += energyRate * rateCoefficient;
		if(energy <= 90){
			changeState(LittlePeopleStates.sleeping);
		} else if(energy >= maxEnergy) {
			energy = maxEnergy;
			changeState(LittlePeopleStates.idle);
		}
	}

	void adjustHealth(float rateCoefficient = 1f)
	{
		health += healthRate * rateCoefficient;
	}

	void changeState(LittlePeopleStates state)
	{
		if (currentState != LittlePeopleStates.idle && state != LittlePeopleStates.idle ){ return; }
		currentState = state;
	}

	void printStats()
	{
		Debug.Log ("Energy: "+ energy + " Hunger: "+ hunger + " Health: "+ health + "\nCURRENT STATE = "+ currentState);
	}
}
