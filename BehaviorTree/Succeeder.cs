﻿using System.Collections;
using System;
using UnityEngine;

public class Succeeder : Decorator 
{
	public Succeeder()
	{
		ReturnStatus = Status.Success;
		Update = () =>
		{
			if(Children != null && Children.Count > 0)
			{
				Children[0].Tick();
			}
			ReturnStatus = (Parent is Selector ?  Status.Failure : Status.Success);
			return ReturnStatus;
		};
	}
}
