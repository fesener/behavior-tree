﻿using System.Collections;
using System;

public class Decorator : Composite 
{
	public Status ReturnStatus { protected get; set; }
	public Func<bool> _canRun { protected get; set; }

	public Decorator()
	{
		ReturnStatus = Status.Failure;
		Update = () =>
		{
			if(_canRun != null && _canRun() && Children != null && Children.Count > 0)
			{
				return Children[0].Tick();
			}
			return ReturnStatus;
		};
	}

	public Decorator CanRun(Func<bool> canRun)
	{
		_canRun = canRun;
		return this;
	}
}
