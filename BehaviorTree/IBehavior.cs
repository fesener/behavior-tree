﻿using System;
using System.Collections;



public interface IBehavior 
{
	Status Status { get; set; }
	IBehavior Parent { get; set; }

	Action Initialize { set; }
	Func<Status> Update { set; }
	Action<Status> Terminate { set; }

	Status Tick();
	void Reset();
}
