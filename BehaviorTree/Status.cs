﻿using System.Collections;

public enum Status 
{
	Invalid,
	Success,
	Failure,
	Running
}
