﻿using System.Collections;

public class Sequence : Composite
{
	protected int _sequence;

	public Sequence()
	{
		Update = () => 
		{
			for(;;)
			{
				Status s = GetChild(_sequence).Tick();
				if(s != Status.Success)
				{
					if(s == Status.Failure)
					{
						_sequence = 0;
						return s;
					}
				}
				if(++_sequence == ChildCount)
				{
					_sequence = 0;
					return Status.Success;
				}
			}
		};
		Initialize = () => 
		{ 
			_sequence = 0; 
			Status = Status.Invalid;
		};
	}

	public override void Reset ()
	{
		Initialize();
	}
}

