﻿using System.Collections;

public class Selector : Composite
{
	protected int _selector;

	public Selector()
	{
		Update = () =>
		{
			for(;;)
			{
				Status s = GetChild(_selector).Tick();
				if( s != Status.Failure)
				{
					if(s == Status.Success)
					{
						_selector = 0;
					}
					return s;
				}
				if(++_selector == ChildCount)
				{
					_selector = 0;
					return Status.Failure;
				}
			}
		};

		Initialize = () => 
		{ 
			_selector = 0;
			Status = Status.Invalid;
		};

	}

	public override void Reset()
	{
		Initialize();
	}
}
