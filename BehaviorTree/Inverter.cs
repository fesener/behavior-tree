﻿using UnityEngine;
using System.Collections;

public class Inverter : Decorator 
{

    public Inverter()
    {
        ReturnStatus = Status.Success;
        Update = () =>
        {
            if(Children != null && Children.Count > 0)
            {
                ReturnStatus = Children[0].Tick();
            }

           	if(ReturnStatus == Status.Success)
			{
				ReturnStatus = Status.Failure;
			} else if(ReturnStatus == Status.Failure)
			{
				ReturnStatus = Status.Success;
			}
            return ReturnStatus;
        };
    }
}
