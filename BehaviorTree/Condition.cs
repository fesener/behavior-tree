﻿using System.Collections;
using System;

public class Condition : Behavior
{
	public Status ReturnStatus { protected get; set; }
	public Func<bool> _canRun { protected get; set; }

	public Condition()
	{
		ReturnStatus = Status.Failure;
		Update = () => 
		{
			if(_canRun != null && _canRun())
			{
				return Status.Success;
			}
			return ReturnStatus;
		};

	}

	public Condition CanRun(Func<bool> canRun)
	{
		_canRun = canRun;
		return this;
	}
}
