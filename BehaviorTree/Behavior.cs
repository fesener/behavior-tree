﻿using System;
using System.Collections;

public class Behavior : IBehavior 
{
	public Status Status { get; set; }
	public IBehavior Parent { get; set; }
	public Action Initialize { protected get; set; }
	public Func<Status> Update { protected get; set; }
	public Action<Status> Terminate { protected get; set; }

	public Status Tick()
	{
		if(Status == Status.Invalid && Initialize != null)
		{
			Initialize();
		}

		Status = Update();

		if(Status != Status.Running && Terminate != null)
		{
			Terminate(Status);
		}

		return Status;
	}

	public virtual void Reset() {}
}
