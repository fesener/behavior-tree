﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Hunger : Sequence {

	public int hunger { set; get; }

	private LittlePeople _littlePeople;
	private GameManager _manager;
	private Transform _targetPlaceToEat;

	private int _hungerIncreaseRate;
	private int _hungerDecreaseRate;
	private int _hungerSearchForFoodLimit;
	private int _hungerSatisfactionLimit;


	public Hunger(LittlePeople littlePeople, GameManager manager, int hungerIncreaseRate = 1, int hungerDecreaseRate = 10, int hungerSearchForFoodLimit = 50, int hungerSatisfactionLimit = 0)
	{
		_littlePeople = littlePeople;
		_manager = manager;
		_targetPlaceToEat = null;

		_hungerIncreaseRate = hungerIncreaseRate;
		_hungerDecreaseRate = hungerDecreaseRate;
		_hungerSearchForFoodLimit = hungerSearchForFoodLimit;
		_hungerSatisfactionLimit = hungerSatisfactionLimit;



		Add<Condition>().CanRun(isHungry);
		Add<Behavior>().Update = locateFood;
		Add<Condition>().CanRun(nearFood);
		Add<Behavior>().Update = eatFood;
	}



	public bool isHungry()
	{
		if(_littlePeople.currentStatus == LittlePeopleStatus.EATING && hunger >= _hungerSatisfactionLimit) 
		{
			return true;
		}
		return hunger >= _hungerSearchForFoodLimit;
	}

	public Status locateFood()
	{
		if(_targetPlaceToEat == null)
		{
			Transform targetPlace = _manager.findClosestPlaceToEat(_littlePeople.transform.position);
			if(targetPlace == null)
			{
				return Status.Failure;
			}
			_targetPlaceToEat = targetPlace;
			_littlePeople.transform.parent.GetComponent<Movement>().setTarget(_targetPlaceToEat.position);
			_littlePeople.transform.parent.GetComponent<Movement>().setTargetBuilding(_targetPlaceToEat);
			_littlePeople.currentStatus = LittlePeopleStatus.WALKING;
            return Status.Success;
        }
        return Status.Success;
    }
    
  

	public bool nearFood()
	{
		if ((_littlePeople.transform.parent.transform.position - _targetPlaceToEat.transform.position).magnitude <= 0.25f)
		{
			return true;
		}
		return false;
	}


	public Status eatFood()
	{
		_littlePeople.currentStatus = LittlePeopleStatus.EATING;
		_targetPlaceToEat = null;
		hunger -= _hungerDecreaseRate;
		_littlePeople.setHunger(hunger);

		if(hunger <= _hungerSatisfactionLimit)
		{
			_littlePeople.currentStatus = LittlePeopleStatus.IDLE;
			return Status.Success;
		}

		return Status.Running;
	}

	
}
