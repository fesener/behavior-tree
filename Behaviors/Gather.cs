﻿using UnityEngine;
using System.Collections;

public class Gather : Sequence 
{
	private LittlePeople _littlePeople;
	private GameManager _manager;
	private Storage storage;

	private Inventory inventory;
	private Transform targetForest;
	private Transform targetHome;

	public Gather(LittlePeople littlePeople, GameManager manager)
	{
		_littlePeople = littlePeople;
		_manager = manager;



//		Add<Condition>().CanRun(canGather);
//		Selector gatherSelector = new Selector();
//
//		Selector woodSelector = new Selector();
//
//		Sequence woodSequence1 = new Sequence();
//		woodSequence1.Add<Condition>().CanRun(needWood);
//		woodSequence1.Add<Behavior>().Update = locateForest;
//
//		woodSequence1.Add<Sequence>()
//		woodSequence1.Add<Condition>().CanRun(nearForest);
//		woodSequence1.Add<Behavior>().Update = gatherWood;
//		woodSequence1.Add<Condition>().CanRun(gatheredEnough);
//
//		Sequence woodSequence2 = new Sequence();
//		woodSequence2.Add<Behavior>().Update = returnHome;
//		woodSequence2.Add<Condition>().CanRun(nearHome);
//		woodSequence2.Add<Behavior>().Update = deliverGoods;
//
//		woodSelector.Add(woodSequence1);
//		woodSelector.Add(woodSequence2);
//
//		gatherSelector.Add(woodSelector);
//		Add (gatherSelector);


		Add<Condition>().CanRun(canGather);
		Add<Condition>().CanRun(needWood);
		Selector woodRoute = new Selector();

		Sequence returnHomeSequence = new Sequence();
		returnHomeSequence.Add<Condition>().CanRun(gatheredEnough);
		returnHomeSequence.Add<Behavior>().Update = returnHome;
		returnHomeSequence.Add<Condition>().CanRun(nearHome);
		returnHomeSequence.Add<Behavior>().Update = deliverGoods;

		Sequence gatherWoodSequence = new Sequence();
		gatherWoodSequence.Add<Inverter>().Add<Condition>().CanRun(gatheredEnough);
		gatherWoodSequence.Add<Behavior>().Update = locateForest;
		gatherWoodSequence.Add<Condition>().CanRun(nearForest);
		gatherWoodSequence.Add<Behavior>().Update = gatherWood;
		gatherWoodSequence.Add<Condition>().CanRun(gatheredEnough);

		woodRoute.Add(returnHomeSequence);
		woodRoute.Add(gatherWoodSequence);
		Add (woodRoute);
	}


	bool canGather()
	{
		if(_littlePeople.home == null) return false;
		storage =  _littlePeople.home.GetComponent<Storage>();
		if(storage == null) return false;
		return _littlePeople.hunger < _littlePeople.hungerSearchForFoodLimit && _littlePeople.energy > 10 && _littlePeople.currentStatus != LittlePeopleStatus.RESTING && _littlePeople.currentStatus != LittlePeopleStatus.EATING && _littlePeople.home != null;
	}

	bool needWood()
	{
		if(storage.wood < 30)
			return true;
		return false;
	}

	Status locateForest()
	{

		if(targetForest == null)
		{
			Transform targetPlace = _manager.findClosestForest(_littlePeople.transform.position);
			if(targetPlace == null)
			{
				return Status.Failure;
			}
			targetForest = targetPlace;
		}

		_littlePeople.transform.parent.GetComponent<Movement>().setTarget(targetForest.position);
		_littlePeople.transform.parent.GetComponent<Movement>().setTargetBuilding(targetForest);
		_littlePeople.currentStatus = LittlePeopleStatus.WALKING;
		return Status.Success;
	}

	bool nearForest()
	{
		if ((_littlePeople.transform.parent.transform.position - targetForest.position).sqrMagnitude <= 0.25f)
		{
			return true;
		}
		return false;
	}

	Status gatherWood()
	{
		inventory = _littlePeople.GetComponent<Inventory>();
		_littlePeople.currentStatus = LittlePeopleStatus.WORKING;
		inventory.wood++;
		return Status.Success;
	}

	bool gatheredEnough()
	{
		inventory = _littlePeople.GetComponent<Inventory>();
		if(inventory.wood >= 10)
		{
			return true;
		}
		return false;
	}

	Status returnHome()
	{
		if(targetHome == null)
		{
			Transform targetPlace = _littlePeople.home;
			if(targetPlace == null)
			{
				return Status.Failure;
			}
			targetHome = targetPlace;

		}
		_littlePeople.transform.parent.GetComponent<Movement>().setTarget(targetHome.position);
		_littlePeople.transform.parent.GetComponent<Movement>().setTargetBuilding(targetHome);
		_littlePeople.currentStatus = LittlePeopleStatus.WALKING;
		return Status.Success;
	}
	
	bool nearHome()
	{
		if ((_littlePeople.transform.parent.transform.position - targetHome.position).sqrMagnitude <= 0.25f)
		{
			return true;
		}
		return false;
	}

	Status deliverGoods()
	{
		inventory = _littlePeople.GetComponent<Inventory>();
		storage.wood += inventory.wood;
		inventory.wood=0;
		return Status.Success;
	}


}
