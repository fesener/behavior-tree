﻿using UnityEngine;
using System.Collections;

public class Initial : Sequence 
{
	private LittlePeople _littlePeople;
	private GameManager _manager;


	Vector3 targetPosition;
	GameObject house;

	public Initial(LittlePeople littlePeople, GameManager manager)
	{
		_littlePeople = littlePeople;
		_manager = manager;


		Add<Inverter>().Add<Condition>().CanRun(haveHome);


		Selector selector = new Selector();
		Add (selector);
		selector.Add<Behavior>().Update = findSomewhereToLive;

		Sequence sequence = new Sequence();
		sequence.Add<Behavior>().Update = findHomePosition;
		sequence.Add<Condition>().CanRun(nearTargetPosition);
		sequence.Add<Behavior>().Update = buildHome;
		selector.Add(sequence);



	}

	bool haveHome()
	{
		return _littlePeople.home != null;
	}

	Status findSomewhereToLive()
	{
		if(_manager.placesToLive.Count != 0)
		{
			for(int i=0;i<_manager.placesToLive.Count;i++)
			{
				if(_manager.placesToLive[i].GetComponent<LittleHouse>().moveToHouse(_littlePeople.transform))
				{
					_littlePeople.home = _manager.placesToLive[i];
					return Status.Success;
				}
			}
		}
		return Status.Failure;
	}

	Status findHomePosition()
	{
		if(targetPosition == Vector3.zero)
		{
			if(_manager.forests.Count > 0)
			{
				Vector2 randomPoint = Random.insideUnitCircle * 10;
				targetPosition = _manager.forests[Random.Range(0, _manager.forests.Count)].position + new Vector3(randomPoint.x, 0 , randomPoint.y);
			} else
			{
				targetPosition = Random.insideUnitCircle * 10;
			}
		}
		_littlePeople.transform.parent.GetComponent<Movement>().target = targetPosition;
		_littlePeople.transform.parent.GetComponent<Movement>().setTargetBuilding(null);
		_littlePeople.currentStatus = LittlePeopleStatus.WALKING;
		return Status.Success;
	}

	bool nearTargetPosition()
	{
		if ((_littlePeople.transform.position - targetPosition).magnitude <= 1f)
		{
			return true;
		} 
		return false;
	}

	Status buildHome()
	{
		house = (GameObject)GameObject.Instantiate(Resources.Load("Tent",typeof(GameObject)), new Vector3(_littlePeople.transform.position.x, 0, _littlePeople.transform.position.z), Quaternion.identity);
		_littlePeople.home = house.transform;
		return Status.Success;
	}
}
