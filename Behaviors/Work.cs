﻿using UnityEngine;
using System.Collections;

public class Work : Sequence {

	private LittlePeople _littlePeople;
	private GameManager _manager;

	public Work(LittlePeople littlePeople, GameManager manager)
	{
		_littlePeople = littlePeople;
		_manager = manager;

		Add<Condition>().CanRun(canWork);
		Add<Behavior>().Update = locateWork;
		Add<Condition>().CanRun(nearWork);
		Add<Behavior>().Update = work;
	}

	public Status goToWork()
	{

		_littlePeople.currentStatus = LittlePeopleStatus.WALKING;
		_littlePeople.transform.parent.GetComponent<Movement>().setTarget(_littlePeople.workplace.position);
		_littlePeople.transform.parent.GetComponent<Movement>().setTargetBuilding(_littlePeople.workplace);
		if ((_littlePeople.transform.position - _littlePeople.workplace.position).magnitude <= 0.25f)
		{
			_littlePeople.currentStatus = LittlePeopleStatus.WORKING;
			return Status.Success;
		}
		return Status.Success;
	}

	public Status locateWork()
	{
		if(_littlePeople.workplace != null)
		{
			_littlePeople.transform.parent.GetComponent<Movement>().setTarget(_littlePeople.workplace.position);
			_littlePeople.transform.parent.GetComponent<Movement>().setTargetBuilding(_littlePeople.workplace);
			_littlePeople.currentStatus = LittlePeopleStatus.WALKING;
		}
		return Status.Success;

	}
	
	public bool nearWork()
	{
		if ((_littlePeople.transform.parent.transform.position - _littlePeople.workplace.position).magnitude <= 0.25f)
		{
			return true;
		} 
		return false;
	}

	public Status work()
	{
		_littlePeople.currentStatus = LittlePeopleStatus.WORKING;
		return Status.Success;
	}
	public bool canWork()
	{
		return _littlePeople.hunger < _littlePeople.hungerSearchForFoodLimit && _littlePeople.energy > 10 && _littlePeople.currentStatus != LittlePeopleStatus.RESTING && _littlePeople.currentStatus != LittlePeopleStatus.EATING && _littlePeople.home != null;
	}
}
