﻿using UnityEngine;
using System.Collections;

public class Rest : Sequence {

	private LittlePeople _littlePeople;
	private GameManager _manager;
	public int _energy = 100;
	
	public Rest(LittlePeople littlePeople, GameManager manager)
	{
		_littlePeople = littlePeople;
		_manager = manager;
		//Add<Behavior>().Update = decreaseEnergy;
		Add<Condition>().CanRun(shouldRest);
		Add<Behavior>().Update = locateHome;
		Add<Condition>().CanRun(nearHome);
		Add<Behavior>().Update = sleep;
	}

	Status decreaseEnergy()
	{
		Debug.Log("decreaseenergy"+_littlePeople.currentStatus.ToString());
		if((_littlePeople.currentStatus != LittlePeopleStatus.RESTING))
		{
			if(_energy>0) 
			{
				_energy-=5;
			}
			else { 
				_energy = 0;
			}
			_littlePeople.setEnergy(_energy);
		}
		return Status.Success;
	}

	public Status locateHome()
	{
		if(_littlePeople.home != null)
		{
			_littlePeople.transform.parent.GetComponent<Movement>().setTarget(_littlePeople.home.position);
			_littlePeople.transform.parent.GetComponent<Movement>().setTargetBuilding(_littlePeople.home);
			_littlePeople.currentStatus = LittlePeopleStatus.WALKING;
			return Status.Success;
		}
		return Status.Failure;
	}

	public bool nearHome()
	{

		if ((_littlePeople.transform.parent.transform.position - _littlePeople.home.position).magnitude <= 0.25f)
		{
			return true;
		} 
		return false;
	}

	public Status sleep()
	{
		_littlePeople.currentStatus = LittlePeopleStatus.RESTING;
		_energy+=10;
		_littlePeople.setEnergy(_energy);
		if(_energy >= 100)
		{
			_littlePeople.currentStatus = LittlePeopleStatus.IDLE;
			return Status.Success;
		}
		return Status.Running;
	}


	bool shouldRest()
	{
		if(_littlePeople.currentStatus == LittlePeopleStatus.RESTING) return true;
		return _energy <= 10 && _littlePeople.hunger < _littlePeople.hungerSearchForFoodLimit;
	}
}
