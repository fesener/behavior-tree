﻿using UnityEngine;
using System;
using UnityEngine.UI;

public class LittlePeopleClickHandler : MonoBehaviour {

	public static Action<GameObject> clickAction = null;
	public static LittlePeopleClickHandler selectedPerson = null;
	private GameObject _infoPanel;  
	private GameManager manager;

	void Awake()
	{
		_infoPanel = GameManager.littlePersonInfoPanel;
		manager = (GameManager)GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
	}
	
	void OnMouseDown() {
		if(clickAction != null) {
			clickAction(this.gameObject);
		}
		else {
			selectedPerson = this;
			if(!_infoPanel.activeSelf) _infoPanel.SetActive(true);
			foreach(GameObject person in manager.littlePeople)
			{
				person.transform.FindChild("Selected").gameObject.SetActive(false);
			}
			selectedPerson.transform.parent.transform.FindChild("Selected").gameObject.SetActive(true);


		}
	}
}
