﻿using UnityEngine;
using System.Collections;

public enum LittlePeopleStatus  {
	IDLE,
	EATING,
	WORKING,
	WALKING,
	RESTING,
}
