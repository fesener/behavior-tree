﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	Transform _transform;
	public float speed;
	public Vector3 target;
	public Transform targetBuilding;

	void Awake() 
	{
		_transform = transform;
		target = _transform.position;
	}

	void Update () 
	{
		if(target != null)
		{
			transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
			transform.LookAt(target);
		}
	}

	public void setTarget(Vector3 position)
	{
		target = position;
	}

	public void setTargetBuilding(Transform t)
	{
		targetBuilding = t;
	}
}
