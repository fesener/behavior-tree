﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class LittlePeople : MonoBehaviour 
{
	private GameManager manager;

	private float timer;

	Initial initialSequence;
	Hunger hungerSequence;
	Rest restSequence;
	Work workSequence;
	Gather gatherSequence;

	public int hunger
	{
		get
		{
			return hungerSequence.hunger;
		}
	}


	public int energy
	{
		get
		{
			return restSequence._energy;
		}
	}

	public string realName;
	public List<Behavior> behaviors = new List<Behavior>();
	public float treeUpdateCooldown;


	public int hungerIncreaseRate;
	public int hungerDecreaseRate;
	public int hungerSearchForFoodLimit;

	public Transform workplace;
	public Transform home;

	public LittlePeopleStatus currentStatus { get; set; }
	GameObject warningPrefab;

	string[] possibleNames={"Mahmut 1","Mahmut 2","Mahmut 3", "Mahmut 4", "Mahmut 5", "Mahmut 6"};




	void Awake()
	{
		manager = (GameManager)GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

		if(manager.placesToWork.Count != 0)
		{
			workplace = manager.placesToWork[UnityEngine.Random.Range(0, manager.placesToWork.Count)];
		}



		if(realName.Equals(string.Empty)) realName = possibleNames[UnityEngine.Random.Range(0, possibleNames.Length)];
		warningPrefab = (GameObject)Instantiate(Resources.Load("Warning",typeof(GameObject)));

		warningPrefab.transform.parent = this.transform;
        warningPrefab.transform.position = this.transform.position + new Vector3(0, 2.5f, 0);
        warningPrefab.SetActive(false);
		currentStatus = LittlePeopleStatus.IDLE;


	}

	void Start()
	{

		PrioritySelector selector = new PrioritySelector();

		selector.Add<Behavior>().Update = updateStats;

		initialSequence = new Initial(this, manager);
		selector.Add(initialSequence);

		hungerSequence = new Hunger(this, manager, hungerIncreaseRate, hungerDecreaseRate, hungerSearchForFoodLimit);
		selector.Add(hungerSequence);

		restSequence = new Rest(this,manager);
		selector.Add (restSequence);

		workSequence = new Work(this, manager);
		selector.Add (workSequence);

		gatherSequence = new Gather(this, manager);
		selector.Add(gatherSequence);




		behaviors.Add(selector);
		for(int i=0;i<behaviors.Count;i++)
		{
			behaviors[i].Tick();
		}

	}


	public Status updateStats()
	{

		if((currentStatus != LittlePeopleStatus.EATING))
		{
			hungerSequence.hunger += hungerIncreaseRate;
		}

		int e = restSequence._energy;
		if((currentStatus != LittlePeopleStatus.RESTING))
		{
			if(e>0) 
			{
				e-=5;
			}
			else { 
				e = 0;
			}

			restSequence._energy = e;
		}

		return Status.Failure;
	}



	void Update()
	{
		timer += Time.deltaTime;
		if(timer >= treeUpdateCooldown)
		{
			for(int i=0;i<behaviors.Count;i++)
			{
				behaviors[i].Tick();
			}
			timer = 0;
		}

		if(hunger>=hungerSearchForFoodLimit)
		{
			warningPrefab.SetActive(true);
			warningPrefab.transform.eulerAngles = new Vector3(0,45,0);
		} 
		else
		{
			warningPrefab.SetActive(false);
		}
	}

	


	public void setHunger(int hunger)
	{
		//this.hunger = hunger;
	}
	public void setEnergy(int energy)
	{
		//this.energy = energy;
	}

}
